# install-kicad-extras

Installation instructions to install Kamaro extra KiCAD libraries

## Install KiCAD

See https://kicad.org

## Download Kamaro Libraries

```
# install git
sudo apt update
sudo apt upgrade
sudo apt install git
# prepare installation directory
cd /opt
sudo mkdir -p kamaro-kicad-libraries
sudo chown $USER:$USER kamaro-kicad-libraries
# download libraries
cd /opt/kamaro-kicad-libraries
git clone https://gitlab.com/kamaro-engineering/pcbs/kamaro-symbols.git
git clone https://gitlab.com/kamaro-engineering/pcbs/kamaro-footprints.git
git clone https://gitlab.com/kamaro-engineering/pcbs/kamaro-packages3d.git
git clone https://gitlab.com/kamaro-engineering/pcbs/kamaro-templates.git
cd ~
```

## Register Libraries in KiCAD

## Update

```
cd /opt/kamaro-kicad-libraries
cd kamaro-symbols
git pull
cd ../kamaro-footprints
git pull
cd ../kamaro-packages3d
git pull
cd ../kamaro-templates
git pull
cd ~
```
